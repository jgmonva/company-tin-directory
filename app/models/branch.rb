class Branch < ApplicationRecord
  validates :name, presence: true
  validates :tin_number, presence: true
  belongs_to :branchable, polymorphic: true
end
