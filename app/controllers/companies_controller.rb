class CompaniesController < ApplicationController
  def index
    @companies = Company.all
  end

  def show
    @company = Company.find(params[:id])
  end

  def new
    @company = Company.new
  end

  def create
    @company = Company.new(company_params)

    if @company.save
      redirect_to companies_path, notice: "The company has been successfully created."
    end
  end

  def edit
    @company = Company.find(params[:id])
  end

  def update
    @company = Company.find(params[:id])
    if @company.update_attributes(company_params)
      redirect_to companies_path, notice: "The company has been updated"
    end
  end

  def destroy
    @company = Company.find(params[:id])
    @company.destroy
  end

  private
    def company_params
      params.require(:company).permit!
    end

end
