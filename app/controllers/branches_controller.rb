class BranchesController < ApplicationController
  def index
  end

  def show
  end

  def new
    @branchable = branchable
    @branch = @branchable.branches.new
  end

  def create
    @branchable = branchable
    @branch = @branchable.branches.new(branch_params)

    if @branch.save
      redirect_to branchable_url(branchable), notice: "The branch has been successfully created."
    end
  end

  def edit
    @branchable = branchable
    @branch = branchable.branches.find(params[:id])
  end

  def update
    @branchable = branchable
    @branch = @branchable.branches.find(params[:id])
    if @branch.update_attributes(branch_params)
      redirect_to branchable_url(branchable), notice: "The branch has been updated"
    end
  end

  def destroy
  end

  private
    def branch_params
      params.require(:branch).permit!
    end

    def branchable
      if params[:company_id]
        id = params[:company_id]
        Company.find(params[:company_id])

      end
    end

    def branchable_url(branchable)
      if Company === branchable
        company_path(branchable)
      end
    end

end
