Rails.application.routes.draw do

  resources :companies do
    resources :branches, only: [:new, :create, :edit, :update]
  end
  # get 'branches/index'
  # get 'branches/show'
  # get 'branches/new'
  # get 'branches/create'
  # get 'branches/edit'
  # get 'branches/update'
  # get 'branches/destroy'

  # resources :companies

  # get 'companies/index'
  # get 'companies/show'
  # get 'companies/new'
  # get 'companies/create'
  # get 'companies/edit'
  # get 'companies/update'
  # get 'companies/destroy'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
