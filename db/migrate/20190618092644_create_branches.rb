class CreateBranches < ActiveRecord::Migration[5.2]
  def change
    create_table :branches do |t|
      t.string :name
      t.string :tin_number
      t.references :branchable, polymorphic: true

      t.timestamps
    end
  end
end
